module.exports = {
  entry: './src/app.js',
  output: {
    path: './out',
    filename: 'app.bundle.js'
  },
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    contentBase: 'out',
    inline: true
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015']
      }
    }]
  }
};
